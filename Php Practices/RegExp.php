
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Regular expression in the php</title>

</head>
<body>

<?php

//Regular expression?
//A regular expression is a sequence of characters that forms a search pattern. When you search for data in a text, you can use this search pattern to describe what you are searching for.

//In PHP, regular expressions are strings composed of delimiters, a pattern and optional modifiers.

//$exp = "/freecodedamn/i";

//In the example above, / is the delimiter, w3schools is the pattern 
//that is being searched for, and i is a modifier that makes the search case-insensitive.


//1.preg_match()
//The preg_match() function will tell you whether a string contains matches of a pattern.
//it returns 1 if the string found else it returns 0

$str = "Visiting freecodedamn";

// $pattern ="/freecodecamp/i";
$pattern ="/freecodedamn/i";

echo preg_match($pattern,$str);

//1


echo "<br>";
echo "<br>";
echo "<br>";
//preg_match_all()

//The preg_match_all() function will tell you how many matches were found for a pattern in a string.

$str1="The rain in SPAIN falls mainly on the plains.";

$pattern = "/ain/i";

echo preg_match_all($pattern, $str1);

echo "<br>";
echo "<br>";
echo "<br>";

//4


//preg_replace()

//The preg_replace() function will replace all of the matches of the pattern in a string with another string.

$str2 = "Visit my blog for the awesome programming stuff.";

$pattern = "/blog/i";

echo preg_replace($pattern, "Blog", $str2);

//Visit my Blog for the awesome programming stuff.



?>

    
</body>
</html>