
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>constructor in php</title>
</head>
<body>

<?php

//A constructor allows you to initialize an object's properties upon creation of the object.

//If you create a __construct() function, PHP will automatically call this function when you create an object from a class.

//Notice that the construct function starts with two underscores (__)!


class exa{
    public function __construct() {
        echo "Hello php developers";
    }
}

$obj1=new exa();

//Hello php developers

//other example on constructor

class cars{
    public $carname;
    public $carcolor;
    function __construct($carname,$carcolor){
        $this->name=$carname;
        $this->color=$carcolor;
    }
    function getCar_Name(){
        return $this->name;
    }
    
    function getCar_Color() {
        return $this->color;
    }
}
echo "\n";
$obj = new cars("Mercedes","yellow");
echo $obj->getCar_Name();
echo "\n";
echo $obj->getCar_Color();

//Mercedes
//yellow

/*

class emp{
    public $name;
    public $salary;
    
    function __construct ($name,$salary){
        $this->name = $name;
        $this->salary = $salary;
    }
    
}
echo"\n";
$ob1 = new emp("sk","20000");
echo "The name of employee is: $ob1->name";
echo"\n";
echo "The salary of employee is: $ob1->salary";

*/

//The name of employee is: sk
//The salary of employee is: 20000
echo "<br>";


class emp1{
    public $name;
    public $salary;
    
    function __construct ($name,$salary){
        $this->name = $name;
        $this->salary = $salary;
    }

    function emp_Details() {
        echo "The name of the employee is: $this->name". "  and the salary of employee is : $this->salary";
        // echo "The name of the employee is: {$this->name}. and the salary of employee is : {$this->salary}";
    }
    
}
echo"\n";
$obj1 = new emp1("sk","20000");

$obj1->emp_Details();

//The name of the employee is: sk  and the salary of employee is : 20000

echo "<br>";


//Destruct in php

//If you create a __destruct() function, PHP will automatically call this function at the end of the script.

//destruct function starts with two underscores (__)!

// A __destruct() function that is automatically called at the end of the script.

class emp{
    public $name;
    public $salary;
    
    function __construct ($name,$salary){
        $this->name = $name;
        $this->salary = $salary;
    }
    
    function __destruct(){
     echo "The name of employee is: {$this->name}. and the salary of the employee is: {$this->salary}";
    
    }
}
$ob1 = new emp("sk","20000");

//The name of employee is: sk. and the salary of the employee is: 20000







?>
    
</body>
</html>