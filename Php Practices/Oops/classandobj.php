
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Classes and object in php</title>
</head>
<body>

<?php

//class and objects in php

//classes are almost like functions but with a major different that classes contain both variables and function, in a single format called object or can be said Class is made from a collection of objects
/*
class cars {
    //properties
    var $name;
    var $color;
    
    
    //methods
    
    //$ this keyword refers to the present object, and this keyword can only be used inside a member function of a class.
    function set_carname($name){
        $this->name =$name;
    }
    function get_carname() {
        return $this->name;
    }
    function set_carcolor($color) {
        $this->color = $color;
    }
    function get_carcolor() {
        return $this->color;
    }
}

$xuv = new cars();
$audi = new cars();
$vw = new cars();
$safari = new cars();
$thar = new cars();
$mercedes = new cars();

$xuv->set_carname('xuv');
$xuv->set_carcolor('red');

$audi->set_carname('Audi');
$audi->set_carcolor('Black');

$vw->set_carname("Vw");
$vw->set_carcolor("Green");

$safari->set_carname("Safari");
$safari->set_carcolor("voilet");

$thar->set_carname('Thar');
$thar->set_carcolor('skyblue');

$mercedes->set_carname('Mercedes');
$mercedes->set_carcolor('yellow');

echo $xuv->get_carname();
echo "-->";

echo $xuv->get_carcolor();

echo"\n";

echo $vw->get_carname();
echo "-->";
echo $vw->get_carcolor();
echo"\n";
echo $audi->get_carname();
echo "-->";
echo $audi->get_carcolor();
echo"\n";
echo $safari->get_carname();
echo "-->";
echo $safari->get_carcolor();
echo"\n";
echo $thar->get_carname();
echo "-->";
echo $thar->get_carcolor();
echo"\n";
echo $mercedes->get_carname();
echo "-->";
echo $mercedes->get_carcolor();
echo"\n";
*/

//xuv-->red
//Vw-->Green
//Audi-->Black
//Safari-->voilet
//Thar-->skyblue
//Mercedes-->yellow


//other examples on classs and objects

class emp {
    public $name;
    var $salary;
    function set_name($name) {
        $this->name=$name;
    }
    
    function set_salary($salary){
        $this->salary=$salary;
    }
}

$emp1 = new emp();
$emp1->set_name("santosh");
$emp1->set_salary("  $750  ");

$emp2 = new emp();
$emp2->set_name("khushi");
$emp2->set_salary(" $500 ");

echo $emp1->name;
echo "";
echo $emp1->salary;
echo "\n";
echo $emp2->name;
echo " ";
echo $emp2->salary;


// santosh  $750  
// khushi  $500 

//other examples on class and object

class labourer{
    // member variables
    var $name;
    var $salary;
    var $profile;
    
    //Member functions
    
    function setName($new){
        $this->name = $new;
    }
    function getName() {
        echo $this->name;
    }
    function setSalary($new){
        $this->salary=$new;
    }
    function getSalary(){
        echo $this->salary;
    }
    function setProfile($new){
        $this->profile=$new;
    }
    
    function getProfile(){
        echo $this->profile;
    }
}

$worker1 = new labourer;
$worker2 = new labourer;
$worker3 = new labourer;

$worker1->setName("Johnson");
$worker1->setSalary(" 50000 ");
$worker1->setProfile(" Web developer ");

$worker1->getName();
$worker1->getSalary();
$worker1->getProfile();

echo"\n";

$worker2->setName("Santosh");
$worker2->setSalary(" 30000 ");
$worker2->setProfile(" Junior Web developer ");

$worker2->getName();
$worker2->getSalary();
$worker2->getProfile();

echo"\n";

$worker3->setName("khushbhu");
$worker3->setSalary(" 20000 ");
$worker3->setProfile(" support engineer ");

$worker3->getName();
$worker3->getSalary();
$worker3->getProfile();

//Johnson 50000  Web developer 
//Santosh 30000  Junior Web developer 
//khushbhu 20000  support engineer 



?>
    
</body>
</html>