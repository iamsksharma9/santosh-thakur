
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inheritance in php</title>
</head>
<body>
<?php
//An inherited class is defined by using the extends keyword.

class fruits 
{
    public $name;
    public $color;
    public function __construct($name,$color){
        $this->name=$name;
        $this->color=$color;
    }
    public function show(){
        
        echo " The fruit is {$this->name} and the color is {$this->color}";
    }
}  
    class pineapple extends fruits{
        
        public function message() {
            echo "Am i a fruits or anything else?";
        }
    }


$mango = new pineapple("pineapple","brown");
$mango->message();
echo "<br>";
$mango->show();

//Am i a fruits or anything else?
 //The fruit is pineapple and the color is brown


echo"<br>";


class A {
    function test() {
         $a = 10;
         $b = 20;
     $sum = $a + $b;
    echo "The sum of a and b is:{$sum}";
    }
}  
   class B extends A {
        
       public function show(){
            echo " Hello Php programmers ";
            
        }
    }
    echo"<br>";

    class C extends B {
       public function result() {
            echo " I love web dev ";
           
        }
    }


$obj1=new C();
echo"<br>";
$obj1->test();
echo"<br>";
$obj1->show();
echo"<br>";
$obj1->result();

//The sum of a and b is:30
//Hello Php programmers
//I love web dev
?>
    
</body>
</html>