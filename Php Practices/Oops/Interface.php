
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Interfaces in php</title>
</head>
<body>

<?php

//interfaces

//Interfaces allow you to specify what methods a class should implement.

//Interfaces make it easy to use a variety of different classes in the same way.

//When one or more classes use the same interface, it is referred to as "polymorphism".

interface A {
    public function sound();
}
class B implements A {
    public function sound() {
        
        echo "Meow";
    }
}

$animal = new B();
$animal->sound();

//Meow 



?>
    
</body>
</html>