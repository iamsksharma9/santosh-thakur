
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>constants in php</title>
</head>
<body>

<?php

//Constants cannot be changed once it is declared.
//Class constants can be useful if you need to define some constant data within a class.
//A class constant is declared inside a class with the const keyword.

//Class constants are case-sensitive. However, it is recommended to name the constants in all uppercase letters.
// We can access a constant from outside the class by using the class name followed by the scope resolution operator (::) followed by the constant name,


class hello{
    const Leaving_msg="Thank you for choosing w3 school for the web development";
    
}

echo hello::Leaving_msg;


// we can access a constant from inside the class by using the self keyword followed by the scope resolution operator (::) followed by the constant name, like here

class helloworld{
    const Leaving_msg="Thank you for choosing w3 school for the web development";
    public function goodPractice(){
        echo self::Leaving_msg;
    }
}

$choice = new helloworld();
$choice->goodPractice();


?>
    
</body>
</html>