
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Abstract class in php</title>
</head>
<body>

<?php

//A class that is declared with abstract keywords, is known as abstract class
// in php.It can have abstract and non-abstract methods,it need to be extended
// and its method implemented.Objects of an abstact class cannot be created.

//Abstract method

//A method that is declared as abstract and does not have implementation 
//is known as abstract method

// abstract function dis()  no body

//Rules to create an abstract class

//1.We cannot use abstract classes to instantiate objects directly
//2.objects of an abstract class cannot be created.
//3.The abstract methods of an abstract class must be defined in its subclass
//4.If there is any abstract method in a class , that class must be abstract.
//5.A class be abstract without having abstract method
//6.It is not necessary to declare allmethods in a abstract class
//7.We cannot declare abstract constructors or abstract static methods
//8.If you are extending any abstract class that have
//abstract method,you must
//either provide the implementation of the method
//or make this class abstract



abstract class city {
    function place(){
        echo "We love to visit capital city";
    }
    abstract function village();
}

class capital extends city {
    public function village(){
        echo "Abstract Method";
    }
}

$obj = new capital;
$obj->village();

//Abstract Method



//parent class
abstract class bike {
    public $name;
    public function __construct($name) {
        $this->name = $name;
    }
    abstract public function show() : string;
}

//child class

class honda extends bike {
    
    public function show() : string {
        return"i choose quality ! i am an $this->name !";
    }
}

class shine extends bike {
    public function show() : string {
        return "I am shine lover! I am a $this->name";
    }
}

class splender extends bike {
    public function show() : string {
        return "I am splender lover! I am a $this->name";
    }
}

//create objects from the child classes

$honda = new honda("honda");
echo $honda->show();
echo "\n";

$shine = new shine("Shine");
echo $shine->show();
echo "\n";

$splender = new splender("splender");
echo $splender->show();
echo "\n";

//i choose quality ! i am an honda !
//I am shine lover! I am a Shine
//I am splender lover! I am a splender



abstract class parentclass {
    
    abstract protected function prefix($name);
}

class childclass extends parentclass {
    public function prefix($name,$seperator =".",$greet ="Dear"){
        if($name == "santosh") {
            $prefix = " Mr";
        } else if($name == "khushi") {
            $prefix = " Mrs";
        } else{
            $prefix = "";
        }
        
        return "{$greet}{$prefix}{$seperator}{$name}";
    }
}

$class = new childclass;
echo $class->prefix("santosh");
echo"\n";
echo $class->prefix("khushi");
echo"\n";

//Dear Mr.santosh
//Dear Mrs.khushi



?>
    
</body>
</html>