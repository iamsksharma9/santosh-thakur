
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Callbacks in php</title>
</head>
<body>


<?php

//A callback function (often referred to as just "callback") is a function which is passed as an argument into another function.

//Any existing function can be used as a callback function. 
//To use a function as a callback function, pass a string containing the name of the function as the argument of another function:


function myName() {
    echo "My name is santosh";
}

function myLastname($callback) {
    call_user_func($callback);
}
myLastname('myName');

//code explanation

//in above example, 'myName' is passed as an argument to the 'myLastname'.
//when the 'myLastname' is executed, it calls 'call_user_func' and passes
//the 'myName' as an argument,As a result the 'myName' is executed and outputs the 
//message "My name is santosh"


function myName($callback) {
    call_user_func($callback);
}

$myLastname = function(){
    echo "My first name is santosh and my last name is sharma";
};

myName($myLastname);

//In the example above, the anonymous function is assigned to the variable
//'$myLastname'.This variable can then passed as an argument to the 'myName'.

//My first name is santosh and my last name is sharma


function myCall($item){
    return strlen($item);
    }
    
    $strings = ["santosh","vivek","khushbu"];
    $lengths = array_map("myCall",$strings);
    
    //here array_map() function is used to calculate the length
    //of eery string in the array
    
    print_r($lengths);

/*
(
    [0] => 7
    [1] => 5
    [2] => 7
) */


//Starting with version 7, PHP can pass anonymous functions as callback functions:


$strings = ["santosh","vivek","khushbu"];
$lengths = array_map(function($item) {return strlen($item);},$strings);

//here array_map() function is used to calculate the length
//of eery string in the array

print_r($lengths);

//

Array
(
    [0] => 7
    [1] => 5
    [2] => 7
)



?>
    
</body>
</html>