
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Array in php</title>
</head>
<body>



<?php

//Array()
//An array stores multiple values in one single variable.

//array() function is ued to create an array.

//array()

//There are 3 types of arrays:

//1.Indexed arrays-Arrays with a numeric index
//2.Associative assays-Arrays with named keys
//3.Multidimensional arrays- Array containing one or more arrays


//get the length of the array

//the count() function is used to return the length of an array

$fruits = array("apple","banana","pineapple","mango");
echo count($fruits);

//indexed arrays

//There are two ways to create indexed arrays:

//like this index always starts at 0

//$fruits = array("apple","banana","pineapple","mango");

//or the index can be assigned manually like this:

echo"<br>";
echo"<br>";

/*
$fruits[0] = "apple";
$fruits[1] ="banana";
$fruits[2] ="pineapple";
$fruits[3] ="mango";
*/

// echo "i like  " . $fruits[0] . "," . $fruits[1] . " and " . $fruits[2] . ".";

$fruits = array("apple","banana","pineapple","mango");

$arrlength = count($fruits);

for($w = 0; $w < $arrlength ; $w++) {
    echo $fruits[$w];
    echo "<br>";
}



//Associative arrays

//Associative arrays are arrays that use named keys that you assign to them.

//There are 2 ways to create it.

echo"<br>";
echo"<br>";

$age = array("santosh"=>"23","Ravi"=>"15","khushabu"=>"18");

echo "santosh is " .$age['santosh'] . " years old";
echo"<br>";
echo "khushabu is " .$age['khushabu'] . " years old";

echo"<br>";
echo"<br>";
echo"<br>";

//throught the looping accessing the values

foreach($age as $w => $w_val) {

    echo "key=" . $w . " , val = " .$w_val;
    echo"<br>";
}

//key=Ravi , val = 15
// key=khushabu , val = 18

//other ways to create associative arrays 

/*

$age['santosh'] = "23";

$age['Ravi'] = "15";

$age['khushbu'] = "19";
*/

//Two dimensional arrays

//A two-dimensional array is an array of arrays.
//a 3-dimensional array is an array of arrays of arrays).

echo"<br>";
echo"<br>";
echo"<br>";

/*

$cars = array(
    array("vw","red",19),
    array("volvo","yellow",28),
    array("Bmw","green",60),
    array("audi","green",45),
    array("xuv","red",15)
);

echo $cars[0][0]." :In color " . $cars[0][1] . " ,  sold :" . $cars[0][2] ." . ";
echo"<br>";
echo $cars[1][0]." :In color " . $cars[1][1] . " ,  sold :" . $cars[1][2] ." . ";
echo"<br>";

echo $cars[2][0]." :In color " . $cars[2][1] . " ,  sold :" . $cars[2][2] ." . ";
echo"<br>";

echo $cars[3][0]." :In color " . $cars[3][1] . " ,  sold :" . $cars[3][2] ." . ";
echo"<br>";

echo $cars[4][0]." :In color " . $cars[4][1] . " ,  sold :" . $cars[4][2] ." . ";
echo"<br>";

echo $cars[5][0]." :In color " . $cars[5][1] . " ,  sold :" . $cars[5][2] ." . ";
echo"<br>"; */


$cars = array(
    array("vw","red",19),
    array("volvo","yellow",28),
    array("Bmw","green",60),
    array("audi","green",45),
    array("xuv","red",15)
);

for($row = 0; $row < 5; $row++){
    echo" <p><b>Row Number $row</b></p>";
    echo "<ul>";
    for($col = 0; $col < 3; $col++){
        echo "<li>" .$cars[$row][$col]. "</li>";
    }
    echo "</ul>";
}



















?>
  
  



</body>
</html>