<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Php session</title>

</head>
<body>

<?php

session_start();

$_SESSION["Reg no"] = "18yysb7012";

$_SESSION["Name"] = "Santosh";

echo "The name of the student is: " . $_SESSION["Name"] . "<br>";
echo "The Reg no of the student is: " . $_SESSION["Reg no"] . "<br>";


//Destroy() session


session_start();

//remove all session variables

session_unset();

//destroy the session

session_destroy();
?>
    
</body>
</html>