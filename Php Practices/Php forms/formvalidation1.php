
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form validation</title>
    <style>
        .err{
            color:#ff0000;
        }
    </style>
</head>
<body>
    
<?php

$nameErr = $emailErr = $genderErr = $websiteErr = " ";

$name = $email = $gender = $comment = $website = " ";

if($_SERVER["REQUEST_METHOD"] == "POST") {
    if(empty($_POST["name"])) {
        $nameErr = "Name is required here..";
    } else {
        $name = inputCheck($_POST["name"]);

        if(!preg_match("/^[a-zA-Z-'] * $/",$name)) {

            $nameErr = "Only letters and white space are allowed";
        }

        echo"<br>";
    
    }
     if(empty($_POST["email"])) {
        $emailErr = "Email is required here..";
     } else {
        $email = inputCheck($_POST["email"]);
        if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
            $emailErr = "Invalid email formate";
        }
     }

     echo"<br>";


     if(empty($_POST["website"])) {
        $website = " ";
     } else {
        $website = inputCheck($_POST["website"]);

        if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {

            $websiteErr ="Invalid URL";
        }

     }

     echo"<br>";


     if(empty($_POST["comment"])) {
        $comment = " ";
     } else {
        $comment = inputCheck($_POST["comment"]);
     }
}

function inputCheck($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>


<h3>A Form validation Page</h3>

<p><span class="err">* required field</span></p>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

Name : <input type="text" name="name">
<span class="err"><?php echo $nameErr ;?></span>

<br><br>

E-mail: <input type ="text" name="email">
<span class="err"><?php echo $emailErr ;?></span>
<br><br>


website : <input type="text" name="website">
<span class="err"><?php echo $websiteErr ;?></span>

<br><br><br>

comment :<textarea name="comment" row="5" cols="10"></textarea>

<br><br>

Gender :

<input type="radio" name="gender" value="female">Female
<input type="radio" name="gender" value="male">Male
<input type="radio" name="gender" value="other">Other

<span class="err">* <?php echo $genderErr;?></span>

<br><br><br>

<input type="submit" name="submit" value="submit">

</form>

<?php 

echo "<h3>Your input will be displayed here:";
echo "<br>";
echo $name;
echo "<br>";
echo $email;
echo "<br>";

echo $website;
echo "<br>";

echo $comment;
echo "<br>";

echo $gender;


?>



</body>
</html>