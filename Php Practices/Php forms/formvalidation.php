
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form validation in php</title>
    <style>
        .error{
            color:red;
        }
    </style>
</head>
<body>

<?php

$nameError = $emailError = $genderError = $websiteError = " ";

$name = $email = $gender = $comment = $website = " ";

if ($_SERVER["REQUEST_METHOD"] == "POST"){

    if(empty($_POST["name"])) {
        $nameError ="Name is required";
    }else{
        $name = testInput($_POST["name"]);
    }
    if(empty($_POST["email"])){
        $emailError="Email id is required here..";
    }else{
        $email = testInput($_POST["email"]);
    }
    if(empty($_POST["website"])){
        $websiteError = " ";
    }else{
        $website = testInput($_POST["website"]);
    }

    if(empty($_POST["comment"])){
        $comment = " ";

    }else{
 
        $comment = testInput($_POST["comment"]);
    }

    if(empty($_POST["gender"])){
        $genderError ="Gender is required";
    }else{
        $gender = testInput($_POST["gender"]);
    }

}

function testInput($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>


<h3>This is a form validation page</h3>

<p><span class="error">* required field</span></p>

<form method="post"action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">

Name : <input type="text" name="name">

<span class="error"> * <?php echo $nameError;?></span>

<br><br>

E-mail :<input type="text" name="email">

<span class="error"> * <?php echo $emailError;?></span>

<br><br>

Website : <input type="text" name="website">
<span class="error"><?php echo $websiteError;?></span>

<br><br>

Comment : <textarea name="comment" id="" cols="5" rows="30"></textarea>

<br><br>
<input type="radio" name="gender" value="female">Female
<input type="radio" name="gender" value="male">Male
<input type="radio" name="gender" value="Other">Other

<span class="error">*<?php echo $genderError;?></span>

<br><br><br>

<input type="submit" name="submit" value="Submit">

</form>


<?php

echo "<h3>Your Input will show here</h3>";

echo $name;
echo"<br>";

echo $email;
echo"<br>";

echo $website;
echo"<br>";

echo $comment;
echo"<br>";

echo $gender;
echo"<br>";


?>
    
</body>
</html>