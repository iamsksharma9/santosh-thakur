
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>we will be learning about create and write file on the server</title>

</head>
<body>


<?php

// create file - fopen() 

//The fopen() function is also used to create a file. 
//Maybe a little confusing, but in PHP, a file is created using the same 
//function used to open files.

//if you us fopen() on file that does not exist, it will create it,given
//that the file is opened for writing(w) or appending(a)


// fwrite() file 

//The fwrite() function in PHP is an inbuilt function which is used to write to an open file.


//syntax

//fwrite(file, string, length)



$myfile = fopen("fileone.txt","w");

echo fwrite($myfile,"Web dev is my fabourite stuff that i love to do.I love to explore and extend my knowledge in the web dev.");

echo "<br>";
echo "<br>";
echo fwrite($myfile,"Web is one of the best field in it to explore.");

fclose($myfile);

echo "<br>";
echo "<br>";

$myfile = fopen("fileone.txt","r");
echo fread($myfile,filesize("fileone.txt"));

//If an existing file contains some data then
//all the existing data will erashed and we start
//with the empty file and start writing on it.


//syntax for fread()

// $contents = fread($handle, filesize($filename));

echo "<br>";
echo "<br>";
echo "<br>";

$file = fopen("filetwo.txt","w") or die("Unable to open file!");

$txt = "I love to play cricket \n";

fwrite($file,$txt);
$txt ="I love to listen to the song in free time";

fwrite($file,$txt);
fclose($file);

$file = fopen("filetwo.txt","r");

echo fread($file,filesize("filetwo.txt"));

fclose($myfile);

echo"<br>";
echo"<br>";
echo"<br>";


// Append Text

//You can append data to a file by using the "a" mode. 
//The "a" mode appends text to the end of the file, 
//while the "w" mode overrides (and erases) the old content of the file.

$file = fopen("filetwo.txt","a") or die("Unable to open file!");

echo "<br>";
echo "<br>";


$txt =" I love javascript and php for web development";

echo fwrite($file,$txt);

fclose($file); 








?>
    
</body>
</html>