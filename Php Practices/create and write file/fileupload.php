
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>File upload script in php</title>
</head>
<body>

<form action="upload.php" methos="post" enctype="multipart/form-data">

Select the image file to upload :

<input type="file" name="fileToUpload" id="fileToUpload">
<input type="submit" name="Upload image" id="submit">
</form>
    
<?php

$target_dir = "upload/";

$target_file = $target_dir .basename($_FILES["fileToUpload"]["name"]);

$uploadok = 1;

$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

//CHECK IF THE IMAGE IS REAL OR FAKE

id(isset($_POST["submit"])) {

    $check = getimagesize($_FILES["fileToUpload"]["tmp_name"])

    if ($check !== false ) {
        echo "File is an image -" . $check["mime"] . " . ";
        $uploadok = 1;
    } else {
        echo "File is not an image";
        $uploadok = 0;
    }
}

//check if file already exists

if(file_exists($target_file)) {
    echo "Sorry, file already exists .";
    $uploadok = 0;
}

//check file size

if($_FILES["fileToUpload"]["size"] > 500000) {

    echo "sorry, your file is too large.";
    $uploadok = 0;
}

//Allow certain file formates

if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" 
&& $imageFileType != "gif") {
    echo "Sorry, only jpg, jpeg, png and gif files are allowed";
    $uploadok = 0;
}

//check if $uploadok is set to 0 by an error

if($uploadok == 0) {
    echo "Sorry , your file was not supported";
} else {

    if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {

        echo "The file " . htmlspecialchars(basename($_FILES["fileToUpload"] ["name"])). "has been uploaded .";

    } else {

        echo "Sorry , there was an error uploading your file.";
    }
}


//it is not getting run as there were some issues that need to be fixed 
//i will just do couple of file upload example then i will fix it later



?>


</body>
</html>