
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Exception in php</title>
</head>
<body>

<h3>Provide the input and get the result</h3>
        
        <form action="" method="POST">
            
          Enter first Num:  <input type="number" name="x" >
          <br><br>
          Enter second Num:  <input type="number" name="y" >
          <br><br>
          <input type="submit" value="submit" name="submit">

</form>



<?php


/* The PHP $_REQUEST is a PHP superglobal variable that is used to collect the data after 
submitting the HTML forms as the $_REQUEST variable is useful to read the data from the submitted HTML open forms.
$_REQUEST is an associative array that by default contains contents of an $_GET, $_POST, and $_COOKIE.*/

if(isset($_REQUEST['submit'])) {
    $x = $_REQUEST['x'];
    $y = $_REQUEST['y'];
    try {
        if($y<=0){
            throw new exception("Enter value of Y is invalid");
        } else {
            $result = $x * $y;
            echo $result;
        }
    }
    
    catch(Exception $e) {
        echo $e -> getMessage();

        //getMessage() is a method to show any exception throw by the try block
    }
}

// enter the no 12 3
//36



//An exception is an object that describes an error or unexpected behaviour of a PHP script.

//Exceptions are thrown by many PHP functions and classes.

//User defined functions and classes can also throw exceptions.

//Exceptions are a good way to stop a function when it comes across data that it cannot use.

//Each try must have at least one corresponding catch or finaaly block.

//The main purpose of using exception handling is to maintain the normal execution of the application.

//if an exception is not caught, a fetal error will be issued with an "uncaught exception" message.

//syntax : new Exception(message, code, previous)

//message-Optional. A string describing why the exception was thrown
//code-Optional. An integer that can be used used to easily distinguish this exception from others of the same type.
//previous-Optional. If this exception was thrown in a catch block of another exception, it is recommended to pass that exception into this parameter


//When catching an exception, the following table shows some of the methods that can be used to get information about the exception:
//getMessage()-Returns a string describing why the exception was thrown
//getPrevious()-If this exception was triggered by another one, this method returns the previous exception. If not, then it returns null

//getCode()-Returns the exception code
//getFile()-Returns the full path of the file in which the exception was thrown

//getLine()-Returns the line number of the line of code which threw the exception




/*

$x = 10;
 
try{
    if($x >= 10){
       $y = 20;
        echo $z = $x + $y;
        echo "\n";
    } else {
        throw new exception("Please enter the value grether than 10");
    }
}

//if the if condition satisfied then catch will not get executed.
//catch only get run if there is an exception occured in the try block
catch(Exception $e){
    echo $e -> getMessage();
    //getMessage is a function to show any exception thrown 
    
}
//finally will get executed every time regardless of the exception occured or not
finally {
    echo "finally block reached";
}

*/

//30
//finally block reached





?>

    
</body>
</html>