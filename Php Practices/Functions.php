
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Functions in Php</title>
</head>
<body>
    

<?php

//Function() in php

//A function is a block of statements that can be used repeatedly in a program.
//A function will not execute automatically when a page loads.
//A function will be executed by a call to the function.


//user defined function

//A user-defined function declaration starts with the word function

echo"<br>";
echo"<br>";


function sayMyname() {
    echo "<h2>My name is Santosh Sharma</h2>";
}

sayMyname();

echo"<br>";
echo"<br>";

//Other examples of functions in php

function studName($fName,$regNo,$grade) {
    echo "My first name is :$fName. and My reg no is :$regNo and my gradde is :$grade";

}
studName("Santosh","18yysb7012","BCA");
echo"<br>";
studName("Santosh","18yysb7022","BSC");
echo"<br>";
studName("Santosh","18yysb7050","BBA");
echo"<br>";
studName("Santosh","18yysb7100","BCOM");


//php default argument value
//it will take default value if we didn't assigned it.

echo"<br>";
echo"<br>";


function area(int $minarea = 100){
    echo" The area is :$minarea<br>";
}
area(250);
area(300);
area(400);
area();
area();
area(500);


//Returning values

//To let a function return a value, use the return statement

echo"<br>";
echo"<br>";

function multiply($n , $p) {
    $m = $n * $p;
    return $m;
}

echo"5 * 10 =" . multiply(10,5) . "<br>";
echo"5 * 12 =" . multiply(5,12) . "<br>";
echo"5 * 15 =" . multiply(5,15) . "<br>";
echo"5 * 30 =" . multiply(5,30);


//<?php declare(strict_types=1);
// we can put strict to only use same data types if we use 
//mismatch datatypes it will returns an fetal error

echo"<br>";
echo"<br>";
echo"<br>";

function addFloatNo($k,$s):float{

    return $k + $s;

}
echo addFloatNo(3.5,4.4);

//7.9

//similarly for the int

echo"<br>";
echo"<br>";
echo"<br>";

function addTwoNo($A,$B) : int {
    return $A + $B;
}

echo addTwoNo(2.2,3.4);


// Passing Arguments by Reference

//Use a passby reference to update the variable used inside 
//the function while it has been declared while function creation
//we have to use & symbol and passed in the function value to 
//change the values inside the function and do the calculation 


echo"<br>";
echo"<br>";
echo"<br>";

function add_No(&$num) {
    $num +=6;
}

$value = 10;
add_No($value);
echo $value;

//16



?>


</body>
</html>
