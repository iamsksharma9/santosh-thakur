
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Php strings</title>
</head>
<body>

<?php
//1.strlen() 

//strlen() function returns the length of a string.

echo strlen("Hello developer");

echo "<br>";


//str_word_count() 
//This function returns the count no of the words in a string.

echo str_word_count("Hello developer, What's up?");


//strrev()-Reverse a string
//strrev() function reverses a string.

echo "<br>";

echo strrev("Hellow my lord");


//strpos()- Search for a text within a string
//The PHP strpos() function searches for a specific 
//text within a string. If a match is found, the function returns 
//the character position of the first match. If no match is found, 
//it will return FALSE.

echo "<br>";

echo strpos("Hello developers","developers");

//str_replace() - Replace text within a string

echo "<br>";

echo str_replace("santosh","world","Hello santosh");





?>
    
</body>
</html>