<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lopps in php</title>
</head>
<body>


<?php
//Often when you write code, you want the same block of code to run 
//over and over again a certain number of times. So, instead of adding 
//several almost equal code-lines in a script, we can use loops.

//while loop
//The while loop - Loops through a block of code as long as the specified condition is true.



$x = 1;

while($x <= 10){
    echo"The number is : $x";

    $x++;
    echo"<br>";
}


// do-while loop 

//The do...while loop - Loops through a block of code once, and then repeats the loop as long as the specified condition is true.

echo"<br>";

$y = 2;

do{
    echo"The number is:$y";

    // ++$y;
     $y++;
echo"<br>";


} while($y<=5);

echo"<br>";
echo"<br>";


//for loop - Loops through a block of code a specified number of times.

for($z = 0; $z <= 10; $z++){
    echo "The number is: $z <br>";
}

//foreach loop in php

//The foreach loop - Loops through a block of code for each element in an array.

//The foreach loop is used to traverse the array elements. It works only on array and 
//object. It will issue an error if you try to use it with the variables of different datatype.
//The foreach loop is used to traverse the array elements. It works only on array and object. 
//It will issue an error if you try to use it with the variables of different datatype.

echo"<br>";

$colors = array("red","green","blue","yellow");

foreach($colors as $value){
    echo "$value <br>";
}

//over the objects iterating with foreach loops

echo"<br>";

$student = array(
    "Name" => "Ravi",
    "Email" => "Ravi123@gmail.com", 
    "Age" => 23,
    "Gender" => "Male"
);

foreach($student as $key => $element){
    echo $key.":".$element;
    echo"<br>";
}

//break and continue in the while loop use cases

$a=0;

while($a < 10) {
    if($a==8){
        break;
    }
    echo"The number is:$a";
    echo"<br>";

    $a++;
}

echo"<br>";
echo"<br>";

$b = 1;
while($b <= 8){
    if($b==5){
        $b++;
     continue;
    }
    echo"The number is:$b";
    echo"<br>";
    $b++;
}


?>



    
</body>
</html>