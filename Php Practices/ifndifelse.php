<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>If and if else statement in php</title>
</head>
<body>




<?php

// if statement and if -else statement

$weather = "Rain";

if($weather==="Rain"){
    echo "Take the umberalla, Today's, there is a raining.";

}else {

    echo"Don't take the umberalla, Today's, there is no rainining.";
}

// elseif statment 

echo"<br>";
echo"<br>";
echo"<br>";

$marks = 90;

if($marks < 34){
    echo"Failed";
}
elseif($marks >= 35 && $marks < 50){
    echo"You got B grade";
}
elseif($marks > 50 && $marks < 65){
    echo"You got B+ grade";
}
elseif($marks > 65 && $marks  < 80){
    echo "You got A grade";
}
elseif($marks > 80 && $marks <=90){
    echo"You got A+ grade with Excellent performance";
}
elseif($marks > 90 && $marks <= 100){
    echo"You got A+ grade with Outstanding Performance";
}else{
    echo"Invalid input";
}

//nested if statement in php

//syntax

/*

if(condition){
 codr to be executed if the condition is true
if(condition){
 codr to be executed if the condition is true
}
}  */

echo"<br>";
echo"<br>";
echo"<br>";


$age = 18;

$nationality = "Indian";

if($nationality =="Indian"){
    if($age >= 18){
        echo"You can give the vote, You are eligible for it";
    }
    else{
        echo"You can't give the Vote, You are not eligible for it";
    }
}


echo"<br>";
echo"<br>";
echo"<br>";


//Switch statement in php

$color = "green";

switch($color){
    case "red":
        echo"My fabourite color is red";
        break;
    case "green":
        echo"My fabourite color is green";
            break;
    case "blue":
        echo"My fabourite color is blue";
            break;
    case "violet":
        echo"My fabourite color is violet";
            break;
    case "black":
        echo"My fabourite color is black";
            break;    
    default:
        echo"Your fabourite color is neither red,green,blue,voilet nor black";   
}

?>


    
</body>
</html>