

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sorting in php using built in function</title>
</head>
<body>


<?php

//sorting()

//The elements in an array can be sorted in alphabetical or numerical order, descending or ascending.

//sort()-it will sort the arrays in ascending order

$fruits = array("mango","apple","banana","pineapple","litchi");

sort($fruits);

$arrlen = count($fruits);

for($x = 0; $x < $arrlen; $x++){
    echo $fruits[$x];
    echo"<br>";
}


//apple
//banana
//litchi
//mango
//pineapple

//similary we can sort the no stored in the array


//rsort()

//this function is used to sort the element  in the descending order.

echo"<br>";
echo"<br>";
echo"<br>";


$fruit = array("mango","apple","banana","pineapple","litchi");

rsort($fruit);


$lenfruits = count($fruit);

for($y = 0; $y < $lenfruits; $y++){
    echo $fruit[$y];
    echo"<br>";
}

//pineapple
//mango
//litchi
//banana
//apple

// asort()
// sorts an associative array in ascending order. sort acoording to value

echo"<br>";
echo"<br>";

$fruitss = array("mango"=>"25","apple"=>"50","banana"=>"15","pineapple"=>"40","litchi"=>"10");

asort($fruitss);

foreach($fruitss as $z => $z_value){
    echo "key =" . $z . " , value = ".$z_value;
    echo"<br>";
}

//ksort() - This method is used to sort the associative array in ascending order according to the key.


// similarly arsort() method is used to sort the associative array in descending order
//according to the value:

//arsort()- This method is used to sort the associative array in descending order.
//according to the value:


//similary krsort()- This method is used to sort the associative array in descending order
//according to the key.




?>
    
</body>
</html>