
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>include in php</title>
</head>
<body>


<h3>Welcome to my home page!</h3>

<p>Some text</p>

<p>This is a phenominal platform for the coding stuff.
    You can learn a lot of technologies write here..
</p>

<?php include 'footer.php' ;?>

<br><br>

<h4>Here we will be adding footer with the help of required file</h4>

<!-- required file script added here.. -->

<?php require 'footer.php' ;?>



<?php


//include and require statement 
//require - it will produce a fatal error and stop the execution

//include - it will produce a warrning but it will continue to execute the script


//File handling

//File handling is an important part of any web application. You often need to open and process a file for different tasks.


//readfile() function reads a file and writes it to the output buffer.

//The readfile() function is useful if all you 
//want to do is open up a file and read its contents.




echo readfile("readone.txt");


//Open File - fopen()

//A better method to open files is with the fopen() function. This function gives you more options than the readfile() function.

//Read file - fread()

//The fread() function reads from an open file.


//The first parameter of fread() contains the 
//name of the file to read from and the second parameter specifies the maximum number of bytes to read.



fread($myfile,filesize("readone.txt"));


//Close File - fclose()

//The fclose() function is used to close an open file.

$myfile = fopen("readone.txt" ,"r");

fclose($myfile);

echo"<br>";
echo"<br>";

//fgets()
//The fgets() function is used to read a single line from a file.

$myfile = fopen("readone.txt","r") or die("unable to open file!");

echo fgets($myfile);
fclose($myfile);

echo"<br>";
echo"<br>";


//Check End-Of-File - feof()

//The feof() function checks if the "end-of-file" (EOF) has been reached.

//The feof() function is useful for looping through data of unknown length.


$myfile = fopen("readone.txt","r") or die("Unable to open file!");

while(!feof($myfile)) {
    echo fgets($myfile) . "<br>";
}
 fclose($myfile);


 //Read Single Character - fgetc()

 //The fgetc() function is used to read a single character from a file.

 echo "<br>";
 echo "<br>";

 $myfile = fopen("readone.txt","r") or die("Unable to open file!");

 while(!feof($myfile)) {
     echo fgetc($myfile) . "<br>";
 }
  fclose($myfile);


  //below some file are not getting executed because we hae
  //attached require file above so that it is not getting further executed.

  




?>




    
</body>
</html>