
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cookies in the php</title>
</head>
<body>


<?php

// echo "Cookie is created";


//Acessing the cookies

if(isset($_COOKIE["Audition"])) {
    echo "Audition are for the " .$_COOKIE["Audition"];
} else {

    echo "No others are for the audition";
}

echo "<br>";
echo "<br>";
echo "<br>";

//Deleting the cookies

setcookie ("Audition" ," ", time() - 60);

echo"cookie is deleted";


?>

<p>
    <strong>Note:</strong>
    You have to refresh the page to see the value of the cookie.
</p>






<?php

// A cookie is created with the setcookie() function.

//setcookie(name, value, expire, path, domain, secure, httponly);

//Only the name parameter is required. All other parameters are optional.

//Name: It is used to set the name of the cookie.
//Value: It is used to set the value of the cookie


setcookie("Audition","Actor", time() + 2 * 24 * 60 * 60);

?>
    
</body>
</html>