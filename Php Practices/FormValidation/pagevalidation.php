

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>A full validation page in Php</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>


<?php

$fnameEr = $lnameEr = $emailEr = $qualiEr = $mobileEr = $age = $websiteEr = $comment = "";

$fname = $lname = $email= $qualification = $mobile = $age = $website = $comment = "";

if($_SERVER["REQUEST_METHOD"]=="POST") {
    if(empty($_POST["fname"])) {
        $fnameEr = "Enter your first name here";
    } else {
        $fname = inputCheck($_POST["fname"]);

        if (!preg_match ("/^[a-zA-z]*$/", $fname) ) {
            $fnameEr ="Only letters and white space are allowed";
        }
    }

    if(empty($_POST["lname"])) {
        $lnameEr = "Enter your last name here";
    } else  {
        $lname = inputCheck($_POST["lname"]);
        if (!preg_match ("/^[a-zA-z]*$/", $lname) ) {
            $lnameEr ="Only letters and white space are allowed";
        }
    }

    if(empty($_POST["email"])){
        $emailEr = "Enter your email id here";
    } else {
        $email = inputCheck($_POST["email"]);
        if(!preg_match("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$^",$email)) {
            $emailEr ="Enter valid email id";
        }
    }

    if(empty($_POST["qualification"])) {
        $qualiEr = "Enter your qualification here";
    } else {
        $qualification = inputCheck($_POST["qualification"]);

        if (!preg_match ("/^[a-zA-z]*$/", $qualification) ) {
            $qualiEr = "Enter your proper qualification here";
        }
    }

    if(empty($_POST["mobile"])) {
        $mobileEr ="Enter your mobile no here";
    } else {

        $mobile = inputCheck($_POST["mobile"]);

        if (!preg_match ("/^[0-9]*$/", $mobile) ){ 
 
            $mobileEr = "Enter only digit between 0 to 9";
    }

    }
    if(empty($_POST["age"])) {
        $age = " ";
    } else {
        $age = inputCheck($_POST["age"]); 

    }

    if(empty($_POST["website"])) {
        $websiteEr = "Enter your website here";
    } else {
        $website = inputCheck($_POST["website"]);

        if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {

            $websiteEr ="Enter valid website here..";
        }
    }

    if(empty($_POST["comment"])) {
        $comment = " ";
    } else {
        $comment = inputCheck($_POST["comment"]); 

        }

    // if(empty(empty($_POST["fname"])) || empty($_POST["lname"]) || empty($_POST["email"]) || empty($_POST["qualification"]) || empty($_POST["number"])|| empty($_POST["age"]) || empty($_POST["website"]) || empty($_POST["comment"])) {
    //     $action = "index1.php";
    //  } else {
    //     $action = "output1.php";
   
    // }
}

function inputCheck($data) {
    $data = trim($data);
    $data = stripcslashes($data);
    $data = htmlspecialchars($data);
    return $data;

}




?>
    
</body>
</html>