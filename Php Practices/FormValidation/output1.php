
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Output page</title>

    <style>
        h3{
            text-align: center;
            margin-top: 45px;
            font-size: 23px;
            margin-right: 129px;
        }
    </style>
</head>
<body>

<?php echo "<h3>You can check your input data below:</h3>";?>
<br><br>
<section>
First Name:<?php echo $fname;?>
<br><br>
Last Name:<?php echo $lname;?>
<br><br>
Email id:<?php echo $email;?>
<br><br>
Qualification:<?php echo $qualification;?>
<br><br>
Mobile No:<?php echo $mobile;?>
<br><br>
Age:<?php echo $age;?>
<br><br>
Website:<?php echo $website;?>
<br><br>
Comments:<?php echo $comment;?>
</section>
    
</body>
</html>