
<?php require_once 'pagevalidation.php'?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>A Full validation page in Php</title>
</head>
<body>


<div class="container">

   <h3 class="header">Fill out the following details of the survey form</h3>

    <form method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']);?>">
     <p><span class="err">* required field</span></p>
        <label for="fname">First Name:</label>
       <span> <input type="text" name="fname" placeholder="Enter your first name"></span>
      <p> <span class="err">*<?php echo $fnameEr ;?></span></p>
     <br><br>
        <label for="lname">Last Name:</label>
        <span><input type="text" name="lname" placeholder="Enter your last name"></span>
        <P><span class="err">*<?php echo $lnameEr ;?></span></p>
        
        <br><br>
        <label for="email">Email id:</label>
        <span><input type="email" name="email" placeholder="Enter your email id here"></span>
       <P> <span class="err">*<?php echo $emailEr;?></span></p>

        <br><br>
        <label for="qualification">Qualification:</label>
        <span> <input type="text" name="qualification" placeholder="Enter your higest qualification"></span>
        <p><span class="err">*<?php echo $qualiEr ;?></span></p>
        <br><br>
        <label for="mobile">Mobile No:</label>
        <span> <input type="number" name="mobile" placeholder="Enter your Mobile no here"></span>
       <p> <span class="err">*<?php echo $mobileEr ;?></span></p>
        <br><br>
        <label for="age">Age:</label>
        <span><input type="number" name="age" placeholder="Enter your age here"></span>
        <p><span class="err"></span></p>
        <br><br>
        <label for="website">Website:</label>
        <span><input type="text" name="website" placeholder="Enter your website here"></span>
        <p><span class="err">*<?php echo $websiteEr ;?></span></p>

        <br><br>
        <label for="comment">Comments:</label>
        <input type="text" name="comment" placeholder="Enter your comments here" class="comment"></span>
        <br><br>
        <div class="btngroup">
        <input type="submit" name="submit" value="Submit" class="btn-1">
        <input type="reset" name="reset" value="Cancel" class="btn-2">
        </div>

       <?php require_once 'output1.php' ?>

        



    </form>
</div>
    
</body>
</html>