

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>constants in Php</title>
</head>
<body>

<?php

//constants in php

//we can create a constants by 2 ways

//1.Using define() function

//2.Using const keyword

//syntax:

// define(name,value,case-insensitive)

define("message","Hello Php developers");
define("MESSAGE","Hello Php developers");
echo MESSAGE;

echo"<br>";


echo message;


// const keyword

//we can use const to create a constant variable in php

echo "<br>";
echo "<br>";
echo "<br>";
const Message = "Hellow full stack developer";

echo Message;

//There is another way to print the value of a constant
//using constant() function instead of using echo statemet.

//constant(name)

echo "<br>";

define("SS","Welcome to my Web dev blog");
echo SS;
echo"<br>";
echo constant("SS");

echo"<br>";
echo"<br>";
echo"<br>";


define("cars",[
    "volvo",
     "volksawgen",
     
     "thar",
     "xuv"
]);
echo cars[0];

echo "<br>";
echo "<br>";
echo "<br>";

//constants are global
//constants are automatically global and can be used across
//the entire script.

define("Myname","Welcome to my new brand blog");

function myName() {
    echo Myname;
}

myName();


?>
    
</body>
</html>