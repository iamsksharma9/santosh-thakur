

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Json in php</title>
</head>
<body>


<?php

//JSON stands for JavaScript Object Notation, and is a syntax for storing and exchanging data.

//PHP has some built-in functions to handle JSON.

//json_encode()

$arr = array('a' => 1, 'b' => 2,'c' => 3);
echo json_encode($arr);

//{"a":1,"b":2,"c":3}

echo"<br>";

//how to encode an associative array into a JSON object:

$ageList = array("santosh"=>23, "Ravi"=> 18, "jonhson"=>25, "khushbu"=>19);

echo json_encode($ageList);

//{"santosh":23,"Ravi":18,"jonhson":25,"khushbu":19}

echo "<br>";

//The json_encode() function is used to encode a value to JSON format.


//json_decode()

//The json_decode() function is used to decode a JSON object into a PHP object or an associative array.

echo "<br>";

$jsonobjarr = '{"santosh":23,"Ravi":18,"jonhson":25,"khushbu":19}';

var_dump(json_decode($jsonobjarr));

//object(stdClass)#1 (4) { ["santosh"]=> int(23) ["Ravi"]=> int(18) ["jonhson"]=> int(25) ["khushbu"]=> int(19) }

var_dump(json_decode($jsonobjarr,true));

//array(4) { ["santosh"]=> int(23) ["Ravi"]=> int(18) ["jonhson"]=> int(25) ["khushbu"]=> int(19) }


//you can encode the PHP indexed array into a JSON array, like this:

$colors = array("Red", "Green", "Blue", "Orange", "Yellow");
 
echo json_encode($colors);

//["Red","Green","Blue","Orange","Yellow"]



//Assign json encoded string to a php variable

$json = '{"santosh":23, "gautam":25, "sunil":30, "bhola":22}';

//decode json data to php associative array

$array = json_decode($json,true);


//how to access the values from a PHP associative array:


echo $array["santosh"];
echo $array["gautam"];
echo $array["sunil"];
echo $array["bhola"];

//23 25 30 22

//loop throught the associative array

foreach($array as $key => $value) {
    echo $key . "=>" . $value ." "; 
}



//decode json data to php object

$obj1 = json_decode($json);

//how to access the values from a PHP object:

echo $obj1->santosh;
echo $obj1->gautam;
echo $obj1->sunil;
echo $obj1->bhola;





//loop throught the object

foreach($obj1 as $key => $value) {
    echo $key . "=>" . $value . " ";
}

//santosh=>23 gautam=>25 sunil=>30 bhola=>22 santosh=>23 gautam=>25 sunil=>30 bhola=>22 






?>
    
</body>
</html>