
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Php first tutorials</title>
</head>
<body>

<?php

$text = "Hellow Programers";

$text1 = "Hope all are well there";



echo "How are you all the  " .$text. "<br>";



echo "This is my first Php script";

echo'Hii everyone afer long time  ' .$text1. "<br>";

//This is single line comment

//$text2 = "Santosh";

# This is also a single line comment.

# $text3="Hello Santosh";

#Multi-line comments

/* 

this is a multiline comment where you
can comment as much code you want */


$x = 10;
$y = 20;

echo $x;
echo "<br>";
echo $y;

echo "<br>";
$my_Name = "Adil";

echo "My name is $my_Name";

echo "<br>";


$MyFabsite = "Freecode damp";

echo "I love reading programming technical artical on  " . $MyFabsite ."!";

echo "<br>";

$p = 50;

$q = 200;

echo $p + $q;

//Php has three different variable scopes

//local
//global
//static

//Global scope

//A variable declared outside a function has a GLOBAL SCOPE and can only be accessed outside a function:

$a = 10;

function myCal(){

    echo "<h5> Variable a inside the function is : $a</h5>";
}

mycal();

echo "<h5>Variable a outside the function is : $a </h5>";


//Local scope 

// A variable declared within a function has a LOCAL SCOPE and can only be accessed within that function:


function myTest(){

    $b = 30;

    echo "<h5>Variable b inside the function is : $b </h5>";
}

myTest();

echo "<h5>Variable b outside the function is: $b </h5>";

echo "<br>";

//Php global keyword 

//The global keyword is used to access a global variables from within a function

$c = 20;
$d = 40;


function Test() {
    global $c, $d;
    $d = $c + $d;
}

Test();

echo $d;

/*

PHP also stores all global variables in an array called $GLOBALS[index]. 
The index holds the name of the variable. This array is also accessible 
from within functions and can be used to update global variables directly.
*/

echo "<br>";
echo "<br>";
echo "<br>";

$xy = 30;
$yx = 20;

function myTotal() {

    $GLOBALS['yx'] = $GLOBALS['xy'] + $GLOBALS['yx'];
}

myTotal();
echo $yx;

//Static keyword

/* Normally, when a function is completed/executed, all of its 
variables are deleted. However, sometimes we want a local variable 
NOT to be deleted. We need it for a further job */

function myvalue() {
    static $k = 2;
    echo $k;
    $k++;
}
echo "<br>";
myvalue();
echo "<br>";
myvalue();
echo "<br>";
myvalue();


//Data Types

//Variables can store data of different types, and different data types can do different things.

//Php supports following data types:

//1.string
//2.Interger
//3.Float(floating point numbers-also called double)
//4.Boolean
//5.Array
//6.Object
//7.Null
//8.Resource








?>
    
</body>
</html>